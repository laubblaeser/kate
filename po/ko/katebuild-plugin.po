# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2008, 2011, 2012, 2013, 2014, 2015, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2023-10-08 22:50+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "박신조"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde@peremen.name"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "출력"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "다시 빌드"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "취소"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr "진단에 오류와 경고 추가"

#: buildconfig.cpp:37
#, kde-format
msgid "Build & Run"
msgstr "빌드하고 실행"

#: buildconfig.cpp:43
#, kde-format
msgid "Build & Run Settings"
msgstr "빌드하고 실행 설정"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr "빌드"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "대상 선택..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "선택한 대상 빌드"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "선택한 대상 빌드 후 실행"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "정지"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "왼쪽에 있는 탭에 포커스 맞추기"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "오른쪽에 있는 탭에 포커스 맞추기"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "대상 설정"

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr "빌드 정보"

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "빌드할 파일이나 디렉터리가 지정되지 않았습니다."

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"파일 \"%1\"이(가) 로컬 파일이 아닙니다. 로컬 파일만 컴파일할 수 있습니다."

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"명령을 실행할 수 없음: %1\n"
"작업 경로가 없음: %2"

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "\"%1\"을(를) 실행할 수 없습니다. 종료 상태 = %2"

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "<b>%1</b> 빌드 취소됨"

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr "빌드할 대상이 없습니다."

#: plugin_katebuild.cpp:820
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "빌드할 로컬 파일이나 디렉터리가 지정되지 않았습니다."

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr "이미 빌드 중..."

#: plugin_katebuild.cpp:853
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "대상 <b>%1</b> 빌드 중..."

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Make 결과:</title><nl/>%1"

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "<b>%1</b> 빌드가 완료되었습니다. 오류 %2개, 경고 %3개, 메모 %4개"

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "오류 %1개를 발견했습니다."

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "경고 %1개를 발견했습니다."

#: plugin_katebuild.cpp:916
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "메모 %1개를 발견했습니다."

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr "빌드가 실패했습니다."

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr "빌드가 성공적으로 끝났습니다."

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "<b>%1 빌드가 취소되었습니다</b>. 오류 %2개, 경고 %3개, 메모 %4개"

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "실행할 수 없음: %1 작업 디렉터리가 설정되지 않았습니다."

#: plugin_katebuild.cpp:1178
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "오류"

#: plugin_katebuild.cpp:1181
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "경고"

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1187
#, fuzzy, kde-format
#| msgctxt "The same word as 'ld' uses to mark an ..."
#| msgid "undefined reference"
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "정의되지 않은 참조"

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "대상 설정"

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr "청소"

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr "설정"

#: plugin_katebuild.cpp:1224
#, kde-format
msgid "ConfigClean"
msgstr "설정 청소"

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr "빌드 대상을 다음에 저장할 수 없음: %1"

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>대상:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>디렉터리:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"비워 두면 현재 문서의 디렉터리를 사용합니다.\n"
"검색할 디렉터리를 ';' 문자로 구분해서 입력하십시오"

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"사용 방법:\n"
"현재 파일: \"%f\"\n"
"현재 파일의 디렉터리: \"%d\"\n"
"접두사 없는 현재 파일 이름: \"%n\""

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr "프로젝트"

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr "세션"

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "명령/대상 집합 이름"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "작업 디렉터리/명령"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "명령 실행"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr "대상 필터입니다. 화살표 키로 선택하고 Enter 키로 실행"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "새 대상 집합 만들기"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "명령이나 대상 집합 복사"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "현재 대상이나 대상 집합 삭제"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "새 대상 추가"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "선택한 대상 빌드"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "선택한 대상 빌드 후 실행"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "선택한 대상 위로 이동"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "선택한 대상 아래로 이동"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "빌드(&B)"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "삽입 경로"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "삽입할 디렉터리 선택"

#~ msgid "Project Plugin Targets"
#~ msgstr "프로젝트 플러그인 대상"

#~ msgid "build"
#~ msgstr "빌드"

#~ msgid "clean"
#~ msgstr "청소"

#~ msgid "quick"
#~ msgstr "빠르게"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "<b>%1</b> 빌드가 완료되었습니다."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "<b>%1</b> 빌드 중 오류가 발생했습니다."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "<b>%1</b> 빌드 중 경고가 발생했습니다."

#~ msgid "Show:"
#~ msgstr "보기:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "파일"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "줄"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "메시지"

#~ msgid "Next Error"
#~ msgstr "다음 오류"

#~ msgid "Previous Error"
#~ msgstr "이전 오류"

#~ msgid "Show Marks"
#~ msgstr "마커 표시"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>파일을 열 수 없음:</title><nl/>%1<br/>대상 설정에서 작업 디렉터리"
#~ "를 검색 경로에 추가하십시오"

#~ msgid "Error"
#~ msgstr "오류"

#~ msgid "Warning"
#~ msgstr "경고"

#~ msgid "Only Errors"
#~ msgstr "오류만"

#~ msgid "Errors and Warnings"
#~ msgstr "오류와 경고"

#~ msgid "Parsed Output"
#~ msgstr "처리된 출력"

#~ msgid "Full Output"
#~ msgstr "전체 출력"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr "체크 상자를 선택하면 대상 집합의 기본 명령으로 사용합니다."

#~ msgid "Select active target set"
#~ msgstr "활성 대상 집합 선택"

#~ msgid "Filter targets"
#~ msgstr "필터 대상"

#~ msgid "Build Default Target"
#~ msgstr "기본 대상 빌드"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "기본 대상 빌드"

#~ msgid "Build Previous Target"
#~ msgstr "이전 대상 빌드"

#~ msgid "Active target-set:"
#~ msgstr "활성 대상 집합:"

#~ msgid "config"
#~ msgstr "설정"

#~ msgid "Kate Build Plugin"
#~ msgstr "Kate 빌드 플러그인"

#~ msgid "Select build target"
#~ msgstr "빌드 대상 선택"

#~ msgid "Filter"
#~ msgstr "필터"

#~ msgid "Build Output"
#~ msgstr "빌드 출력"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Make 결과:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "다음 대상 집합"

#~ msgid "No previous target to build."
#~ msgstr "빌드할 이전 대상이 없습니다."

#~ msgid "No target set as default target."
#~ msgstr "기본 대상으로 설정한 것이 없습니다."

#~ msgid "No target set as clean target."
#~ msgstr "clean 대상으로 설정한 것이 없습니다."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "대상 \"%1\"을(를) 빌드할 수 없습니다."

#~ msgid "Really delete target %1?"
#~ msgstr "대상 %1을(를) 삭제하시겠습니까?"

#~ msgid "Nothing built yet."
#~ msgstr "빌드한 것이 없습니다."

#~ msgid "Target Set %1"
#~ msgstr "대상 %1 설정됨"

#~ msgid "Target"
#~ msgstr "대상"

#~ msgid "Target:"
#~ msgstr "대상:"

#~ msgid "from"
#~ msgstr "원본"

#~ msgid "Sets of Targets"
#~ msgstr "대상 집합"

#~ msgid "Make Results"
#~ msgstr "Make 결과"

#~ msgid "Others"
#~ msgstr "기타"

#~ msgid "Quick Compile"
#~ msgstr "빠른 컴파일"

#~ msgid "The custom command is empty."
#~ msgstr "사용자 정의 명령이 비어 있습니다."

#~ msgid "New"
#~ msgstr "새로 만들기"

#~ msgid "Copy"
#~ msgstr "복사"

#~ msgid "Delete"
#~ msgstr "삭제"

#~ msgid "Quick compile"
#~ msgstr "빠른 컴파일"
