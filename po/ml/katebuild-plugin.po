# Malayalam translations for kate package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kate package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2018-12-17 02:45+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr ""

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:37
#, kde-format
msgid "Build & Run"
msgstr ""

#: buildconfig.cpp:43
#, kde-format
msgid "Build & Run Settings"
msgstr ""

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr ""

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr ""

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr ""

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr ""

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr ""

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr ""

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr ""

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr ""

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr ""

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr ""

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:820
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr ""

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:853
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr ""

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] ""
msgstr[1] ""

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] ""
msgstr[1] ""

#: plugin_katebuild.cpp:916
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] ""
msgstr[1] ""

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr ""

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr ""

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1178
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr ""

#: plugin_katebuild.cpp:1181
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr ""

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1187
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr ""

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr ""

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr ""

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr ""

#: plugin_katebuild.cpp:1224
#, kde-format
msgid "ConfigClean"
msgstr ""

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr ""

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr ""

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr ""

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr ""

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr ""

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr ""

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr ""

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr ""

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr ""

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr ""

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr ""

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr ""
