# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Stelios <sstavra@gmail.com>, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-05 01:42+0000\n"
"PO-Revision-Date: 2023-01-02 22:53+0200\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:252
#, kde-format
msgid "Filter..."
msgstr "Φιλτράρισμα..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:452 lspclientpluginview.cpp:607 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Πελάτης LSP"

#: lspclientconfigpage.cpp:213
#, kde-format
msgid "No JSON data to validate."
msgstr "Δεν υπάρχουν JSON δεδομένα για επικύρωση."

#: lspclientconfigpage.cpp:222
#, kde-format
msgid "JSON data is valid."
msgstr "Τα δεδομένα JSON είναι έγκυρα."

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Τα δεδομένα JSON δεν είναι έγκυρα: δεν υπάρχει JSON αντικείμενο."

#: lspclientconfigpage.cpp:227
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Τα δεδομένα JSON δεν είναι έγκυρα: %1"

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr "Διαγραφή επιλεγμένων εγγραφών"

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr "Διαγραφή όλων των εγγραφών"

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr "Ζητήθηκε εκκίνηση του εξυπηρετητή LSP"

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""
"Θέλετε να εκκινήσει ο εξυπηρετητής LSP;<br><br>Η πλήρης εντολή είναι:"
"<br><br><b>%1</b><br><br>Η επιλογή μπορεί να αλλάξει από τη σελίδα "
"διαμόρφωσης του προσθέτου."

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""
"Ο χρήστης αποκλείστηκε μόνιμα στην αρχή του: '%1'.\n"
"Αναιρέστε αυτόν τον αποκλεισμό από τη σελίδα διαμόρφωσης του προσθέτου."

#: lspclientpluginview.cpp:419 lspclientpluginview.cpp:671
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:481
#, kde-format
msgid "Go to Definition"
msgstr "Μετάβαση στον ορισμό"

#: lspclientpluginview.cpp:483
#, kde-format
msgid "Go to Declaration"
msgstr "Μετάβαση στη δήλωση"

#: lspclientpluginview.cpp:485
#, kde-format
msgid "Go to Type Definition"
msgstr "Μετάβαση στον ορισμό τύπου"

#: lspclientpluginview.cpp:487
#, kde-format
msgid "Find References"
msgstr "Εύρεση αναφορών"

#: lspclientpluginview.cpp:490
#, kde-format
msgid "Find Implementations"
msgstr "Εύρεση υλοποιήσεων"

#: lspclientpluginview.cpp:492
#, kde-format
msgid "Highlight"
msgstr "Τονισμός"

#: lspclientpluginview.cpp:494
#, kde-format
msgid "Symbol Info"
msgstr "Πληροφορίες συμβόλων"

#: lspclientpluginview.cpp:496
#, kde-format
msgid "Search and Go to Symbol"
msgstr "Αναζήτηση και μετάβαση στο σύμβολο"

#: lspclientpluginview.cpp:501
#, kde-format
msgid "Format"
msgstr "Τύπος"

#: lspclientpluginview.cpp:504
#, kde-format
msgid "Rename"
msgstr "Μετονομασία"

#: lspclientpluginview.cpp:507
#, kde-format
msgid "Expand Selection"
msgstr "Επέκταση επιλογής"

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Shrink Selection"
msgstr "Συρρίκνωση επιλογής"

#: lspclientpluginview.cpp:513
#, kde-format
msgid "Switch Source Header"
msgstr "Εναλλαγή επικεφαλίδας πηγής"

#: lspclientpluginview.cpp:516
#, kde-format
msgid "Expand Macro"
msgstr "Επέκταση μακροεντολής"

#: lspclientpluginview.cpp:518
#, kde-format
msgid "Code Action"
msgstr "Ενέργεια κώδικα"

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Show selected completion documentation"
msgstr "Εμφάνιση επιλεγμένης τεκμηρίωσης"

#: lspclientpluginview.cpp:536
#, kde-format
msgid "Enable signature help with auto completion"
msgstr "Ενεργοποίηση βοήθειας υπογραφής με αυτόματη συμπλήρωση"

#: lspclientpluginview.cpp:539
#, kde-format
msgid "Include declaration in references"
msgstr "Να περιλαμβάνεται η δήλωση σε αναφορές"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:542 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr "Προσθήκη παρενθέσεων με την ολοκλήρωση της συνάρτησης"

#: lspclientpluginview.cpp:545
#, kde-format
msgid "Show hover information"
msgstr "Εμφάνιση πληροφοριών αιώρησης"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:548 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr "Διαμόρφωση στην πληκτρολόγηση"

#: lspclientpluginview.cpp:551
#, kde-format
msgid "Incremental document synchronization"
msgstr "Σταδιακός συγχρονισμός εγγράφων"

#: lspclientpluginview.cpp:554
#, kde-format
msgid "Highlight goto location"
msgstr "Τονισμός θέσης μετάβασης"

#: lspclientpluginview.cpp:563
#, kde-format
msgid "Show Inlay Hints"
msgstr "Εμφάνιση ένθετων συμβουλών"

#: lspclientpluginview.cpp:567
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr "Εμφάνιση ειδοποιήσεων διαγνωστικών"

#: lspclientpluginview.cpp:572
#, kde-format
msgid "Show Messages"
msgstr "Εμφάνιση μηνυμάτων"

#: lspclientpluginview.cpp:577
#, kde-format
msgid "Server Memory Usage"
msgstr "Χρήση μνήμης εξυπηρετητή"

#: lspclientpluginview.cpp:581
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr "Κλείσιμο όλων των δυναμικών καρτελών αναφοράς"

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Restart LSP Server"
msgstr "Επανεκκίνηση εξυπηρετητή LSP"

#: lspclientpluginview.cpp:585
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Επανεκκίνηση όλων των εξυπηρετητών LSP"

#: lspclientpluginview.cpp:596
#, kde-format
msgid "Go To"
msgstr "Μετάβαση σε"

#: lspclientpluginview.cpp:623
#, kde-format
msgid "More options"
msgstr "Περισσότερες επιλογές"

#: lspclientpluginview.cpp:829 lspclientsymbolview.cpp:287
#, kde-format
msgid "Expand All"
msgstr "Επέκταση όλων"

#: lspclientpluginview.cpp:830 lspclientsymbolview.cpp:288
#, kde-format
msgid "Collapse All"
msgstr "Σύμπτυξη όλων"

#: lspclientpluginview.cpp:1033
#, kde-format
msgid "RangeHighLight"
msgstr "RangeHighLight"

#: lspclientpluginview.cpp:1345
#, kde-format
msgid "Line: %1: "
msgstr "Γραμμή: %1: "

#: lspclientpluginview.cpp:1497 lspclientpluginview.cpp:1845
#: lspclientpluginview.cpp:1964
#, kde-format
msgid "No results"
msgstr "Χωρίς αποτέλεσμα"

#: lspclientpluginview.cpp:1556
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Ορισμός: %1"

#: lspclientpluginview.cpp:1562
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Δήλωση: %1"

#: lspclientpluginview.cpp:1568
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Ορισμός τύπου: %1"

#: lspclientpluginview.cpp:1574
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Αναφορές: %1"

#: lspclientpluginview.cpp:1586
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Υλοποίηση: %1"

#: lspclientpluginview.cpp:1599
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Τονισμός: %1"

#: lspclientpluginview.cpp:1623 lspclientpluginview.cpp:1634
#: lspclientpluginview.cpp:1647
#, kde-format
msgid "No Actions"
msgstr "Καμία ενέργεια"

#: lspclientpluginview.cpp:1638
#, kde-format
msgid "Loading..."
msgstr "Φόρτωση..."

#: lspclientpluginview.cpp:1730
#, kde-format
msgid "No edits"
msgstr "Χωρίς επεξεργασίες"

#: lspclientpluginview.cpp:1804
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Μετονομασία"

#: lspclientpluginview.cpp:1805
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Νέο όνομα (προσοχή: δεν αντικαθίστανται όλες οι αναφορές)"

#: lspclientpluginview.cpp:1851
#, kde-format
msgid "Not enough results"
msgstr "Ανεπαρκή αποτελέσματα"

#: lspclientpluginview.cpp:1920
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "Η σχετική επικεφαλίδα/πηγή δεν βρέθηκε"

#: lspclientpluginview.cpp:2116 lspclientpluginview.cpp:2153
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "LSP εξυπηρετητής"

#: lspclientpluginview.cpp:2176
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "LSP πελάτης"

#: lspclientpluginview.cpp:2448
#, fuzzy, kde-format
#| msgid "Restart LSP Server"
msgid "Question from LSP server"
msgstr "Επανεκκίνηση εξυπηρετητή LSP"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "Restarting"
msgstr "Επανεκκινεί"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "NOT Restarting"
msgstr "ΔΕΝ επανεκκινεί"

#: lspclientservermanager.cpp:605
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr "Απρόσμενος τερματισμός εξυπηρετητή ... %1 [%2] [αρχική σελίδα: %3] "

#: lspclientservermanager.cpp:801
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "Αποτυχία εύρεσης του εκτελέσιμου του εξυπηρετητή: %1"

#: lspclientservermanager.cpp:804 lspclientservermanager.cpp:846
#, kde-format
msgid "Please check your PATH for the binary"
msgstr "Ελέγξτε τη ΔΙΑΔΡΟΜΗ για το εκτελέσιμο"

#: lspclientservermanager.cpp:805 lspclientservermanager.cpp:847
#, kde-format
msgid "See also %1 for installation or details"
msgstr "Δείτε και στο %1 για εγκατάσταση ή λεπτομέρειες"

#: lspclientservermanager.cpp:843
#, kde-format
msgid "Failed to start server: %1"
msgstr "Αποτυχία εκκίνησης εξυπηρετητή: %1"

#: lspclientservermanager.cpp:851
#, kde-format
msgid "Started server %2: %1"
msgstr "Ξεκίνησε ο εξυπηρετητής %2: %1"

#: lspclientservermanager.cpp:886
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""
"Αποτυχία συντακτικής ανάλυσης της διαμόρφωσης '%1': δεν υπάρχει JSON "
"αντικείμενο"

#: lspclientservermanager.cpp:889
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr "Αποτυχία συντακτικής ανάλυσης της διαμόρφωσης '%1': %2"

#: lspclientservermanager.cpp:893
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "Αποτυχία ανάγνωσης της διαμόρφωσης του εξυπηρετητή: %1"

#: lspclientsymbolview.cpp:240
#, kde-format
msgid "Symbol Outline"
msgstr "Περίγραμμα συμβόλων"

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Tree Mode"
msgstr "Δενδρική λειτουργία"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Αυτόματη επέκταση δένδρου"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Sort Alphabetically"
msgstr "Αλφαβητική ταξινόμηση"

#: lspclientsymbolview.cpp:284
#, kde-format
msgid "Show Details"
msgstr "Εμφάνιση λεπτομερειών"

#: lspclientsymbolview.cpp:445
#, kde-format
msgid "Symbols"
msgstr "Σύμβολα"

#: lspclientsymbolview.cpp:576
#, kde-format
msgid "No LSP server for this document."
msgstr "Δεν υπάρχει εξυπηρετητής LSP για αυτό το έγγραφο."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Ρυθμίσεις πελάτη"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Format on save"
msgstr "Διαμόρφωση στην αποθήκευση"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Ενεργοποίηση τονισμού σημασιολογίας"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr "Ενεργοποίηση ένθετων συμβουλών"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr "Συμπληρώσεις:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr "Εμφάνιση εμβόλιμων εγγράφων επιλεγμένης συμπλήρωσης"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""
"Να εμφανίζεται η υπογραφή της συνάρτησης με την πληκτρολόγηση της κλήσης της"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr "Να προστίθενται εισαγωγές αυτόματα αν το απαιτεί η συμπλήρωση"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr "Πλοήγηση:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""
"Να γίνεται καταμέτρηση των δηλώσεων με την αναζήτηση αναφορών σε σύμβολο"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Να εμφανίζονται πληροφορίες για το τρέχον σε αιώρηση σύμβολο"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Να τονίζεται η γραμμή προορισμού με την αναπήδηση σε αυτήν"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr "Εξυπηρετητής:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, kde-format
msgid "Show program diagnostics"
msgstr "Εμφάνιση διαγνωστικών προγράμματος"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "Να εμφανίζονται ειδοποιήσεις από τον εξυπηρετητή LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "Να γίνεται βαθμιαίος συγχρονισμός των εγγράφων με τον εξυπηρετητή LSP"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr "Περίγραμμα εγγράφου:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Αλφαβητική ταξινόμηση συμβόλων"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr "Να εμφανίζονται επιπλέον λεπτομέρειες για σύμβολα"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr "Να παρουσιάζονται τα σύμβολα ιεραρχικά αντί σε επίπεδη λίστα"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr "Αυτόματη επέκταση δένδρου"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr "Επιτρεπόμενοι && αποκλεισμένοι εξυπηρετητές"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr "Ρυθμίσεις εξυπηρετητή χρήστη"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "Αρχείο ρυθμίσεων:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr "Προκαθορισμένες ρυθμίσεις εξυπηρετητή"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "Περισσότερες επιλογές"

#~ msgid "Quick Fix"
#~ msgstr "Γρήγορη διόρθωση"

#~ msgid "Show Diagnostics Highlights"
#~ msgstr "Εμφάνιση τονισμών διαγνωστικών"

#~ msgid "Show Diagnostics Marks"
#~ msgstr "Εμφάνιση σημείων διαγνωστικών"

#~ msgid "Show Diagnostics on Hover"
#~ msgstr "Εμφάνιση διαγνωστικών σε αιώρηση"

#~ msgid "Switch to Diagnostics Tab"
#~ msgstr "Εναλλαγή στην καρτέλα διαγνωστικών"

#~ msgctxt "@title:tab"
#~ msgid "Diagnostics"
#~ msgstr "Διαγνωστικά"

#~ msgid "Error"
#~ msgstr "Σφάλμα"

#~ msgid "Warning"
#~ msgstr "Προειδοποίηση"

#~ msgid "Information"
#~ msgstr "Πληροφορίες"

#~ msgctxt "@info"
#~ msgid ""
#~ "Error in regular expression: %1\n"
#~ "offset %2: %3"
#~ msgstr ""
#~ "Σφάλμα σε κανονική έκφραση: %1\n"
#~ "μετατόπιση %2: %3"

#~ msgid "Copy to Clipboard"
#~ msgstr "Αντιγραφή στο πρόχειρο"

#~ msgid "Remove Global Suppression"
#~ msgstr "Αφαίρεση καθολικής συμπίεσης"

#~ msgid "Add Global Suppression"
#~ msgstr "Προσθήκη καθολικής συμπίεσης"

#~ msgid "Remove Local Suppression"
#~ msgstr "Αφαίρεση τοπικής συμπίεσης"

#~ msgid "Add Local Suppression"
#~ msgstr "Προσθήκη τοπικής συμπίεσης"

#~ msgid "Disable Suppression"
#~ msgstr "Απενεργοποίηση συμπίεσης"

#~ msgid "Enable Suppression"
#~ msgstr "Ενεργοποίηση συμπίεσης"

#~ msgctxt "@info"
#~ msgid "%1 [suppressed: %2]"
#~ msgstr "%1 [έγινε συμπίεση: %2]"

#~ msgid "Diagnostics:"
#~ msgstr "Διαγνωστικά:"

#~ msgid "Highlight lines with diagnostics"
#~ msgstr "Τονισμός γραμμών που περιέχουν διαγνωστικά"

#~ msgid "Show markers in the margins for lines with diagnostics"
#~ msgstr ""
#~ "Εμφάνιση δεικτών στα περιθώρια για γραμμές που περιέχουν διαγνωστικά"

#~ msgid "Show diagnostics on hover"
#~ msgstr "Εμφάνιση διαγνωστικών σε αιώρηση"

#~ msgid "max diagnostics tooltip size"
#~ msgstr "μέγιστο μέγεθος υποδείξεων διαγνωστικών"

#~ msgid "Type to filter through symbols in your project..."
#~ msgstr "Πληκτρολογήστε για φιλτράρισμα των συμβόλων στο έργο σας..."

#~ msgid "LSP Client Symbol Outline"
#~ msgstr "Περίγραμμα συμβόλων πελάτη LSP"

#~ msgid "General Options"
#~ msgstr "Γενικές επιλογές"

#~ msgid "Add highlights"
#~ msgstr "Προσθήκη τονισμών"

#~ msgid "Add markers"
#~ msgstr "Προσθήκη δεικτών"

#~ msgid "On hover"
#~ msgstr "Σε αιώρηση"

#~ msgid "Tree mode outline"
#~ msgstr "Περίγραμμα δενδρικής λειτουργίας"

#~ msgid "Automatically expand nodes in tree mode"
#~ msgstr "Αυτόματη επέκταση κόμβων σε δενδρική λειτουργία"

#~ msgid "Switch to messages tab upon message level"
#~ msgstr "Εναλλαγή στην καρτέλα μηνυμάτων με το επίπεδο μηνυμάτων"

#~ msgctxt "@info"
#~ msgid "Never"
#~ msgstr "Ποτέ"

#~ msgctxt "@info"
#~ msgid "Error"
#~ msgstr "Σφάλμα"

#~ msgctxt "@info"
#~ msgid "Warning"
#~ msgstr "Προειδοποίηση"

#~ msgctxt "@info"
#~ msgid "Information"
#~ msgstr "Πληροφορίες"

#~ msgctxt "@info"
#~ msgid "Log"
#~ msgstr "Καταγραφή"

#~ msgid "Switch to messages tab"
#~ msgstr "Εναλλαγή στην καρτέλα μηνυμάτων"

#~ msgctxt "@title:tab"
#~ msgid "Messages"
#~ msgstr "Μηνύματα"

#~ msgctxt "@info"
#~ msgid "Unknown"
#~ msgstr "Άγνωστο"

#~ msgid "Switch to messages tab upon level"
#~ msgstr "Εναλλαγή στην καρτέλα μηνυμάτων με το επίπεδο"

#~ msgid "Never"
#~ msgstr "Ποτέ"

#~ msgid "Log"
#~ msgstr "Καταγραφή"

#~ msgid "Hover"
#~ msgstr "Αιώρηση"

#~ msgctxt "@info"
#~ msgid "<b>LSP Client:</b> %1"
#~ msgstr "<b>Πελάτης LSP :</b> %1"

#~ msgid "Server Configuration"
#~ msgstr "Διαμόρφωση εξυπηρετητή"

#, fuzzy
#~| msgid "Format"
#~ msgid "Form"
#~ msgstr "Τύπος"

#~ msgid "Format on typing (newline)"
#~ msgstr "Διαμόρφωση στην πληκτρολόγηση (newline)"
