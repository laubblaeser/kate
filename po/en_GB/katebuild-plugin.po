# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2009, 2010, 2011.
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2023-04-22 11:01+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrew Coles, Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrew_coles@yahoo.co.uk, steve.allewell@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Output"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Build again"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Cancel"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:37
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build & Run"
msgstr "Build again"

#: buildconfig.cpp:43
#, fuzzy, kde-format
#| msgid "Build and Run Selected Target"
msgid "Build & Run Settings"
msgstr "Build and Run Selected Target"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr "Build"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "Select Target..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "Build Selected Target"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Build and Run Selected Target"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Stop"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "Focus Next Tab to the Left"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "Focus Next Tab to the Right"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Target Settings"

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr "Build Information"

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "There is no file or directory specified for building."

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Cannot run command: %1\n"
"Work path does not exist: %2"

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Failed to run \"%1\". exitStatus = %2"

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Building <b>%1</b> cancelled"

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr "No target available for building."

#: plugin_katebuild.cpp:820
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "There is no local file or directory specified for building."

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr "Already building..."

#: plugin_katebuild.cpp:853
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Building target <b>%1</b> ..."

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Make Results:</title><nl/>%1"

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Found one error."
msgstr[1] "Found %1 errors."

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Found one warning."
msgstr[1] "Found %1 warnings."

#: plugin_katebuild.cpp:916
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Found one note."
msgstr[1] "Found %1 notes."

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr "Build failed."

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr "Build completed without problems."

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Build <b>%1 cancelled</b>. %2 error(s), %3 warning(s), %4 note(s)"

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "Cannot execute: %1 No working directory set."

#: plugin_katebuild.cpp:1178
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "error"

#: plugin_katebuild.cpp:1181
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "warning"

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "note|info"

#: plugin_katebuild.cpp:1187
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "undefined reference"

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "Target Set"

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr "Clean"

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr "Config"

#: plugin_katebuild.cpp:1224
#, kde-format
msgid "ConfigClean"
msgstr "ConfigClean"

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>T:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Dir:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "Command/Target-set Name"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "Working Directory / Command"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "Run Command"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr "Filter targets, use arrow keys to select, Enter to execute"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Create new set of targets"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Copy command or target set"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Delete current target or current set of targets"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Add new target"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Build selected target"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Build and run selected target"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Move selected target up"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Move selected target down"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Build"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Insert path"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Select directory to insert"

#~ msgid "Project Plugin Targets"
#~ msgstr "Project Plugin Targets"

#~ msgid "build"
#~ msgstr "build"

#~ msgid "clean"
#~ msgstr "clean"

#~ msgid "quick"
#~ msgstr "quick"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Building <b>%1</b> completed."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Building <b>%1</b> had errors."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Building <b>%1</b> had warnings."

#~ msgid "Show:"
#~ msgstr "Show:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "File"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Line"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Message"

#~ msgid "Next Error"
#~ msgstr "Next Error"

#~ msgid "Previous Error"
#~ msgstr "Previous Error"

#~ msgid "Show Marks"
#~ msgstr "Show Marks"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"

#~ msgid "Error"
#~ msgstr "Error"

#~ msgid "Warning"
#~ msgstr "Warning"

#~ msgid "Only Errors"
#~ msgstr "Only Errors"

#~ msgid "Errors and Warnings"
#~ msgstr "Errors and Warnings"

#~ msgid "Parsed Output"
#~ msgstr "Parsed Output"

#~ msgid "Full Output"
#~ msgstr "Full Output"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr ""
#~ "Tick the tick-box to make the command the default for the target-set."

#~ msgid "Select active target set"
#~ msgstr "Select active target set"

#~ msgid "Filter targets"
#~ msgstr "Filter targets"

#~ msgid "Build Default Target"
#~ msgstr "Build Default Target"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Build Default Target"

#~ msgid "Build Previous Target"
#~ msgstr "Build Previous Target"

#~ msgid "Active target-set:"
#~ msgstr "Active target-set:"

#~ msgid "config"
#~ msgstr "config"

#~ msgid "Kate Build Plugin"
#~ msgstr "Kate Build Plugin"

#~ msgid "Select build target"
#~ msgstr "Select build target"

#~ msgid "Filter"
#~ msgstr "Filter"

#~ msgid "Build Output"
#~ msgstr "Build Output"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Make Results:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Next Set of Targets"

#~ msgid "No previous target to build."
#~ msgstr "No previous target to build."

#~ msgid "No target set as default target."
#~ msgstr "No target set as default target."

#~ msgid "No target set as clean target."
#~ msgstr "No target set as clean target."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Target \"%1\" not found for building."

#~ msgid "Really delete target %1?"
#~ msgstr "Really delete target %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Nothing built yet."

#~ msgid "Target Set %1"
#~ msgstr "Target Set %1"

#~ msgid "Target"
#~ msgstr "Target"

#~ msgid "Target:"
#~ msgstr "Target:"

#~ msgid "from"
#~ msgstr "from"

#~ msgid "Sets of Targets"
#~ msgstr "Sets of Targets"

#~ msgid "Make Results"
#~ msgstr "Make Results"

#~ msgid "Quick Compile"
#~ msgstr "Quick Compile"

#~ msgid "The custom command is empty."
#~ msgstr "The custom command is empty."

#~ msgid "New"
#~ msgstr "New"

#~ msgid "Copy"
#~ msgstr "Copy"

#~ msgid "Delete"
#~ msgstr "Delete"

#~ msgid "Quick compile"
#~ msgstr "Quick compile"

#~ msgid "Run make"
#~ msgstr "Run make"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Break"
#~ msgstr "Break"

#, fuzzy
#~| msgid "There is no file or directory specified for building."
#~ msgid "There is no file to compile."
#~ msgstr "There is no file or directory specified for building."
