# translation of katekonsoleplugin.po to Polish
# translation of katekonsoleplugin.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Michał Smoczyk <msmoczyk@wp.pl>, 2007, 2008, 2010.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2008.
# Michal Smoczyk <msmoczyk@wp.pl>, 2009.
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2014, 2017, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-04-15 11:32+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"Nie masz wystarczających uprawnień aby uzyskać dostęp do powłoki lub "
"terminalu"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:654
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Potok do terminalu"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "Z&synchronizuj terminal z bieżącym dokumentem"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Uruchom bieżący dokument"

#: kateconsole.cpp:156 kateconsole.cpp:503
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Pokaż okno &terminala"

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Przejdź do okna terminala"

#: kateconsole.cpp:302
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr "Konsola nie jest wgrana. Wgraj konsolę, aby móc używać terminala."

#: kateconsole.cpp:378
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Naprawdę chcesz przekazać potokiem tekst do terminalu? Wszystkie polecenia w "
"nim zawarte zostaną wykonane z twoimi prawami użytkownika."

#: kateconsole.cpp:379
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Potok do terminalu?"

#: kateconsole.cpp:380
#, kde-format
msgid "Pipe to Terminal"
msgstr "Potok do terminalu"

#: kateconsole.cpp:408
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Wybacz, ale nie można zmienić katalogu na '%1'"

#: kateconsole.cpp:444
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Plik nielokalny: '%1'"

#: kateconsole.cpp:477
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Czy na pewno chcesz uruchomić ten dokument?\n"
"Wykona to następujące polecenie,\n"
"z uprawnieniami użytkownika w terminalu:\n"
"'%1'"

#: kateconsole.cpp:484
#, kde-format
msgid "Run in Terminal?"
msgstr "Wykonać w terminalu?"

#: kateconsole.cpp:485
#, kde-format
msgid "Run"
msgstr "Wykonaj"

#: kateconsole.cpp:500
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Ukryj okno terminala"

#: kateconsole.cpp:511
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Wyjdź z okna terminala"

#: kateconsole.cpp:512 kateconsole.cpp:513
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Przejdź do okna terminala"

#: kateconsole.cpp:587
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"S&amoczynnie synchronizuj terminal z bieżącym dokumentem, kiedy jest to "
"możliwe"

#: kateconsole.cpp:591 kateconsole.cpp:612
#, kde-format
msgid "Run in terminal"
msgstr "Wykonaj w terminalu"

#: kateconsole.cpp:593
#, kde-format
msgid "&Remove extension"
msgstr "Usuń &rozszerzenie"

#: kateconsole.cpp:598
#, kde-format
msgid "Prefix:"
msgstr "Przedrostek:"

#: kateconsole.cpp:606
#, kde-format
msgid "&Show warning next time"
msgstr "Następnym razem pokaż o&strzeżenie"

#: kateconsole.cpp:608
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"Przy następnym wykonywaniu '%1', upewnij się, że zostanie pokazane "
"ostrzeżenie, wyświetlające polecenie wysyłane do terminala celem "
"zatwierdzenia."

#: kateconsole.cpp:619
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Ustaw zmienną środowiskową &EDITOR na \"kate -b\""

#: kateconsole.cpp:622
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Ważne: dokument musi zostać zamknięty, aby umożliwić kontynuowanie programu "
"terminalu"

#: kateconsole.cpp:625
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Ukryj Konsolę po naciśnięciu 'Esc'"

#: kateconsole.cpp:628
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Może to powodować kłopoty z programami terminala, które wykorzystują klawisz "
"Esc, np. vim. Dodaj te programy w polu poniżej (spis oddzielony przecinkami)"

#: kateconsole.cpp:659
#, kde-format
msgid "Terminal Settings"
msgstr "Ustawienia terminalu"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Narzędzia"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Michał Smoczyk, Marta Rybczyńska, Łukasz Wojniłowicz"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr ""
#~ "msmoczyk@wp.pl, kde-i18n@rybczynska.net, lukasz.wojnilowicz@gmail.com"

#~ msgid "Kate Terminal"
#~ msgstr "Kate Terminal"

#~ msgid "Terminal Panel"
#~ msgstr "Panel terminala"

#~ msgid "Konsole"
#~ msgstr "Konsola"

#~ msgid "Embedded Konsole"
#~ msgstr "Wbudowana konsola"
